import { HttpClient , HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable()
export class ApiService {
  private apiUrl: string = environment.API_URL;

  constructor(private httpClient: HttpClient, private router: Router) { }

  paging<T>(url: string, params: any): Observable<T> {
    return this.httpClient.get<T>(this.apiUrl + url, { params, headers: this.getHeaders() })
      .pipe(
        catchError(error => this.handleError(error))
      );
  }

  get<T>(url: string): Observable<T> {
    return this.httpClient.get<T>(this.apiUrl + url, { headers: this.getHeaders() })
      .pipe(
        catchError(error => this.handleError(error))
      );
  }

  post<T>(url: string, param: any): Observable<T> {
    return this.httpClient.post<T>(this.apiUrl + url, param, { headers: this.getHeaders() })
      .pipe(
        catchError(error => this.handleError(error))
      );
  }

  put<T>(url: string, param: any): Observable<T> {
    return this.httpClient.put<T>(this.apiUrl + url, param, { headers: this.getHeaders() })
      .pipe(
        catchError(error => this.handleError(error))
      );
  }

  delete<T>(url: string): Observable<T> {
    return this.httpClient.delete<T>(this.apiUrl + url, { headers: this.getHeaders() })
      .pipe(
        catchError(error => this.handleError(error))
      );
  }

  private getHeaders(): HttpHeaders {
    const headers = new HttpHeaders();
    return headers.append('Authorization', localStorage.getItem('authorization'));
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      if (error.status === 401) {
        localStorage.removeItem('user');
        this.router.navigate(['/']);
      } else {
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
    }
    // Return an observable with a user-facing error message.
    return throwError('Ah ocurrido un error en el servidor');
  }
}
