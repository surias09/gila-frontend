import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MotorcycleRoutingModule } from './motorcycle-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MotorcycleListComponent } from './motorcycle-list/motorcycle-list.component';
import { MotorcycleFormComponent } from './motorcycle-form/motorcycle-form.component';


@NgModule({
  declarations: [MotorcycleListComponent, MotorcycleFormComponent],
  imports: [
    CommonModule,
    MotorcycleRoutingModule,
    NgxPaginationModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class MotorcycleModule { }
