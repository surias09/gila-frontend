import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MotorcycleFormComponent } from './motorcycle-form/motorcycle-form.component';
import { MotorcycleListComponent } from './motorcycle-list/motorcycle-list.component';

const routes: Routes = [{
  path: '',
  component: MotorcycleListComponent
},
{
  path: 'new', component: MotorcycleFormComponent
},
{
  path: ':id/edit', component: MotorcycleFormComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MotorcycleRoutingModule { }
