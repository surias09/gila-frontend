import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Motorcycle } from 'src/app/models/vehicle-model';
import { ApiService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-motorcycle-form',
  templateUrl: './motorcycle-form.component.html',
  styleUrls: ['./motorcycle-form.component.scss']
})
export class MotorcycleFormComponent implements OnInit {
  private api = '/api/motorcycle/';

  public form: FormGroup;
  public id: string;
  public submitted = false;
  public model: Motorcycle;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder
  ) {
    this.form = fb.group({
      model: ['', [Validators.required]],
      year: ['', [Validators.required]],
      color: ['', [Validators.required]],
      motor: [0, [Validators.required]],
      active: [true]
    });
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      this.get(this.id);
    }
  }

  cancel(): void {
    this.back();
  }

  submit(): void {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    this.model = this.form.getRawValue();
    this.id ? this.update(this.model) : this.save(this.model);
  }

  private get(id: string): void {
    this.apiService.get<Motorcycle>(`${this.api}${id}`)
      .subscribe(response => {
        this.form.patchValue(response);
      });
  }

  private save(model: Motorcycle): void {
    this.apiService.post(`${this.api}`, model)
      .subscribe(() => {
        this.back();
      });
  }

  private update(model: Motorcycle): void {
    model.id = this.id;
    this.apiService.put(`${this.api}`, model)
      .subscribe(() => {
        this.back();
      });
  }

  private back(): void {
    this.id ? this.router.navigate(['../../'], { relativeTo: this.route }) : this.router.navigate(['../'], { relativeTo: this.route });
  }
}
