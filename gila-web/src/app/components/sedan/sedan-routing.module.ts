import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SedanFormComponent } from './sedan-form/sedan-form.component';
import { SedanListComponent } from './sedan-list/sedan-list.component';

const routes: Routes = [{
  path: '',
  component: SedanListComponent
},
{
  path: 'new', component: SedanFormComponent
},
{
  path: ':id/edit', component: SedanFormComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SedanRoutingModule { }
