import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SedanRoutingModule } from './sedan-routing.module';
import { SedanListComponent } from './sedan-list/sedan-list.component';
import { ApiService } from 'src/app/services/api-service.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SedanFormComponent } from './sedan-form/sedan-form.component';


@NgModule({
  declarations: [SedanListComponent, SedanFormComponent],
  imports: [
    CommonModule,
    SedanRoutingModule,
    NgxPaginationModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [ApiService]
})
export class SedanModule { }
