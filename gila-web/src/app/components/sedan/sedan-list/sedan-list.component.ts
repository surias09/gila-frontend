import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageSedan } from 'src/app/models/page-model';
import { VehicleDto } from 'src/app/models/vehicle-model';
import { ApiService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-sedan-list',
  templateUrl: './sedan-list.component.html',
  styleUrls: ['./sedan-list.component.scss']
})
export class SedanListComponent implements OnInit {

  private api = '/api/sedan/';
  public items: VehicleDto[] = [];
  public model: string;

  public page = 1;
  public count = 0;
  public pageSize = 10;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.pagination();
  }

  new(): void {
    this.router.navigate(['./', 'new'], { relativeTo: this.route });
  }

  search(): void {
    this.pagination();
  }

  clean(): void {
    this.model = '';
    this.pagination();
  }

  handlePageChange(event: number): void {
    this.page = event;
    this.pagination();
  }

  edit(item: VehicleDto): void {
    this.router.navigate(['./', item.id, 'edit'], { relativeTo: this.route });
  }

  delete(item: VehicleDto): void {
    this.apiService.delete(`${this.api}${item.id}`)
      .subscribe(() => {
        this.pagination();
      });
  }

  private pagination(): void {
    this.apiService.paging<any>(`${this.api}pagination`, this.getRequestParams(this.model, this.page, this.pageSize))
      .subscribe(response => {
        this.items = response.content;
        this.count = response.totalElements;
      });
  }

  private getRequestParams(model: string, page: number, pageSize: number): any {
    const params: PageSedan = {
      page: page - 1,
      size: pageSize,
      model: model ?? ''
    };

    return params;
  }
}
