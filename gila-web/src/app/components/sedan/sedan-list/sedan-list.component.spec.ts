import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SedanListComponent } from './sedan-list.component';

describe('SedanListComponent', () => {
  let component: SedanListComponent;
  let fixture: ComponentFixture<SedanListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SedanListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SedanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
