import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SedanFormComponent } from './sedan-form.component';

describe('SedanFormComponent', () => {
  let component: SedanFormComponent;
  let fixture: ComponentFixture<SedanFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SedanFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SedanFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
