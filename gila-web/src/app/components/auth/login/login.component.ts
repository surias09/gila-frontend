import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;
  public submitted = false;
  public submitting = false;

  constructor(
    fb: FormBuilder,
    private router: Router,
    private apiService: ApiService
  ) {
    this.form = fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {}

  submit(): void {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    this.apiService.post('/login', this.form.getRawValue())
      .subscribe((response: any) => {
        localStorage.setItem('authorization', response.Authorization);
        this.router.navigate(['/sedan']);
      });
  }
}
