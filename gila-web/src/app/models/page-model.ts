export interface Page {
  page: number;
  size: number;
}

export interface PageSedan extends Page {
  model: string;
}

export interface PageMoto extends Page {
  model: string;
}
