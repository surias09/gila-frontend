export interface VehicleDto {
  id: string;
  model: string;
  year: number;
  color: string;
  wheel: number;
}

export interface Vehicle {
  id: string;
  model: string;
  year: number;
  color: string;
  active: boolean;
}

export interface Sedan extends Vehicle {
  motor: SedanCylinder;
}

export interface Motorcycle extends Vehicle {
  motor: MotoCylinder;
}

export enum SedanCylinder {
  Three,
  Four,
  Six
}

export enum MotoCylinder {
  One,
  Two,
  Three
}
